/****************************************************
 * Description: ServiceImpl for t_mall_panel_content
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.mall.entity.PanelContentEntity;
import com.xjj.mall.dao.PanelContentDao;
import com.xjj.mall.service.PanelContentService;

@Service
public class PanelContentServiceImpl extends XjjServiceSupport<PanelContentEntity> implements PanelContentService {

	@Autowired
	private PanelContentDao panelContentDao;

	@Override
	public XjjDAO<PanelContentEntity> getDao() {
		
		return panelContentDao;
	}
}