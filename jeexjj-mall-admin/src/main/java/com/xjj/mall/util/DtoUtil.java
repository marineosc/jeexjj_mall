package com.xjj.mall.util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xjj.mall.common.pojo.ZTreeNode;
import com.xjj.mall.entity.ItemCatEntity;
import com.xjj.mall.entity.ItemEntity;
import com.xjj.mall.entity.MemberEntity;
import com.xjj.mall.entity.OrderItemEntity;
import com.xjj.mall.entity.PanelEntity;
import com.xjj.mall.pojo.CartProduct;
import com.xjj.mall.pojo.ItemDto;
import com.xjj.mall.pojo.Member;
import com.xjj.mall.pojo.MemberDto;
import com.xjj.mall.pojo.Product;

/**
 * @author Exrick
 * @date 2017/8/25
 */
public class DtoUtil{

    private final static Logger log= LoggerFactory.getLogger(DtoUtil.class);

    public static MemberEntity MemberDto2Member(MemberDto memberDto){

        MemberEntity tbMember =new MemberEntity();

        if(!memberDto.getUsername().isEmpty()){
            tbMember.setUsername(memberDto.getUsername());
        }
        if(!memberDto.getPassword().isEmpty()){
            tbMember.setPassword(memberDto.getPassword());
        }
        if(!memberDto.getPhone().isEmpty()){
            tbMember.setPhone(memberDto.getPhone());
        }
        if(!memberDto.getEmail().isEmpty()){
            tbMember.setEmail(memberDto.getEmail());
        }
        if(!memberDto.getSex().isEmpty()){
            tbMember.setSex(memberDto.getSex());
        }
        if(!memberDto.getDescription().isEmpty()){
            tbMember.setDescription(memberDto.getDescription());
        }
        if(!memberDto.getProvince().isEmpty()){
            tbMember.setAddress(memberDto.getProvince()+" "
                    +memberDto.getCity()+" "+memberDto.getDistrict());
        }

        return tbMember;
    }

    public static ItemEntity ItemDto2TbItem(ItemDto itemDto){

    	ItemEntity tbItem =new ItemEntity();

        tbItem.setTitle(itemDto.getTitle());
        tbItem.setPrice(itemDto.getPrice());
        tbItem.setCid(itemDto.getCid());
        tbItem.setImage(itemDto.getImage());
        tbItem.setSellPoint(itemDto.getSellPoint());
        tbItem.setNum(itemDto.getNum());
        if(itemDto.getLimitNum()==null||itemDto.getLimitNum()<0){
            tbItem.setLimitNum(10);
        }else{
            tbItem.setLimitNum(itemDto.getLimitNum());
        }

        return tbItem;
    }

    public static ItemDto TbItem2ItemDto(ItemEntity tbItem){

        ItemDto itemDto =new ItemDto();

        itemDto.setTitle(tbItem.getTitle());
        itemDto.setPrice(tbItem.getPrice());
        itemDto.setCid(tbItem.getCid());
        itemDto.setImage(tbItem.getImage());
        itemDto.setSellPoint(tbItem.getSellPoint());
        itemDto.setNum(tbItem.getNum());
        if(tbItem.getLimitNum()==null){
            itemDto.setLimitNum(tbItem.getNum());
        }else if(tbItem.getLimitNum()<0&&tbItem.getNum()<0) {
            itemDto.setLimitNum(10);
        }else{
            itemDto.setLimitNum(tbItem.getLimitNum());
        }

        return itemDto;
    }


    public static ZTreeNode TbPanel2ZTreeNode(PanelEntity tbPanel){

        ZTreeNode zTreeNode =new ZTreeNode();

        zTreeNode.setId(tbPanel.getId());
        zTreeNode.setIsParent(false);
        zTreeNode.setpId(0L);
        zTreeNode.setName(tbPanel.getName());
        zTreeNode.setSortOrder(tbPanel.getSortOrder());
        zTreeNode.setStatus(tbPanel.getStatus());
        zTreeNode.setRemark(tbPanel.getRemark());
        zTreeNode.setLimitNum(tbPanel.getLimitNum());
        zTreeNode.setType(tbPanel.getType());

        return zTreeNode;
    }


    public static ZTreeNode TbItemCat2ZTreeNode(ItemCatEntity tbItemCat){

        ZTreeNode zTreeNode =new ZTreeNode();

        zTreeNode.setId(tbItemCat.getId());
        zTreeNode.setStatus(tbItemCat.getStatus());
        zTreeNode.setSortOrder(tbItemCat.getSortOrder());
        zTreeNode.setName(tbItemCat.getName());
        zTreeNode.setpId(tbItemCat.getParentId());
        zTreeNode.setIsParent(tbItemCat.getIsParent().intValue()==1?true:false);
        zTreeNode.setRemark(tbItemCat.getRemark());

        return zTreeNode;
    }

    public static Product TbItem2Product(ItemEntity tbItem){

        Product product =new Product();

        product.setProductId(tbItem.getId());
        product.setProductName(tbItem.getTitle());
        product.setSalePrice(tbItem.getPrice());
        product.setSubTitle(tbItem.getSellPoint());
        if(null != tbItem.getImage() && tbItem.getImage().contains(",http"))
        {
        	product.setProductImageBig(tbItem.getImage().split(",")[0]);
        }

        return product;
    }

    public static Member TbMemer2Member(MemberEntity tbMemer){

        Member member =new Member();

        member.setId(tbMemer.getId());
        member.setUsername(tbMemer.getUsername());
        member.setEmail(tbMemer.getEmail());
        member.setPhone(tbMemer.getPhone());
        member.setAddress(tbMemer.getAddress());
        member.setBalance(tbMemer.getBalance());
        member.setFile(tbMemer.getFile());
        member.setPoints(tbMemer.getPoints());
        member.setSex(tbMemer.getSex());
        member.setDescription(tbMemer.getDescription());

        return member;
    }

    public static CartProduct TbItem2CartProduct(ItemEntity tbItem){

        CartProduct cartProduct =new CartProduct();

        cartProduct.setProductId(tbItem.getId());
        cartProduct.setProductName(tbItem.getTitle());
        cartProduct.setSalePrice(tbItem.getPrice());
        cartProduct.setProductImg(tbItem.getImage());
        if(tbItem.getLimitNum()==null){
            cartProduct.setLimitNum(Long.valueOf(tbItem.getNum()));
        }else if(tbItem.getLimitNum()<0&&tbItem.getNum()<0) {
            cartProduct.setLimitNum((long) 10);
        }else{
            cartProduct.setLimitNum(Long.valueOf(tbItem.getLimitNum()));
        }
        return cartProduct;
    }

    public static CartProduct TbOrderItem2CartProduct(OrderItemEntity tbOrderItem){

        CartProduct cartProduct=new CartProduct();
        cartProduct.setProductId(Long.valueOf(tbOrderItem.getItemId()));
        cartProduct.setProductName(tbOrderItem.getTitle());
        cartProduct.setSalePrice(tbOrderItem.getPrice());
        cartProduct.setProductNum(Long.valueOf(tbOrderItem.getNum()));
        cartProduct.setProductImg(tbOrderItem.getPicPath());

        return cartProduct;
    }
}
